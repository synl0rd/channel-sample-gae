from django.conf.urls.defaults import *
from django.conf import settings

urlpatterns = patterns('chat.views',
    url(r'^$', 'main', name='main'),
    url(r'^channel/$', 'channel_test', name='channel_test'),
)


